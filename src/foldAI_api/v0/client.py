"""Simple client for foldAI REST API."""
from datetime import datetime, timezone, timedelta
from enum import Enum
from typing import Union

import requests
from requests import HTTPError
from functools import lru_cache


DEFAULT_HOST = "https://api.fold.ai"


class HTTPMethod(Enum):
    GET = 0
    POST = 1
    PUT = 2
    HEAD = 3
    DELETE = 4
    PATCH = 5
    OPTIONS = 6


class Client:
    def __init__(self, user_email: str, user_password: str, host: str=DEFAULT_HOST):
        self.user_email = user_email
        self.user_password = user_password
        self.host = host
        self._access_token = self._login()
        self._default_deployment_id = None

    def _login(self):
        """Login to API with email and password, and return access token."""
        resp = requests.post(
            f"{self.host}/v0/login", json={"email": self.user_email, "password": self.user_password}
        )
        resp_json = resp.json()
        if resp.status_code != 200:
            msg = resp_json["msg"]
            raise ConnectionError(msg)
        self._access_token = resp_json["access_token"]
        return self._access_token

    def _request_with_token(self, url, params=None, method: HTTPMethod = HTTPMethod.GET):
        """Send request accompanied by access token. If access token is None or expired, login and get new one.

        :param url: the api endpoint (including possible arguments)
        :param params: url parameters
        :param method: the HTTP method (e.g. POST, GET)
        :return: json response of endpoint
        :raise: HTTPError if response status different from 200
        """
        params = {} if params is None else params

        if self._access_token is None:
            self._access_token = self._login()

        token_header = {"Authorization": f"Bearer {self._access_token}"}
        if method == HTTPMethod.POST:
            resp = requests.post(url, headers=token_header, params=params)
        elif method == HTTPMethod.GET:
            resp = requests.get(url, headers=token_header, params=params)
        else:
            raise NotImplementedError

        resp_status, resp_json = resp.status_code, resp.json()

        if resp_status == 401 and resp_json["msg"] == "Token has expired":
            self._access_token = self._login()
            return self._request_with_token(url=url, method=method, params=params)

        if resp_status != 200:
            raise HTTPError(f"Response status code {resp_status}.")

        return resp_json

    def _get_with_cursor(self, url, params):
        results = []
        data_dict = self._request_with_token(
            url,
            method=HTTPMethod.GET,
            params=params
        )
        results.extend(data_dict["data"])
        cursor = data_dict["paging"]["cursors"]["after"]
        while cursor != "":
            params["cursor"] = cursor
            data_dict = self._request_with_token(
                url,
                method=HTTPMethod.GET,
                params=params
            )
            cursor = data_dict["paging"]["cursors"]["after"]
            results.extend(data_dict["data"])
        return results

    def set_default_deployment_id(self, deployment_id: str):
        """Set deployment as default for this Client's requests.

        :param deployment: deployment id to set as default
        """
        deployments = [d["id"] for d in self.get_deployments()]
        if deployment_id not in deployments:
            raise ValueError("Deployment does not exist or current user can't access it.")
        self._default_deployment_id = deployment_id

    def get_default_deployment_id(self):
        """Return default deployment id for this Client."""
        if self._default_deployment_id is None:
            deployments = [d["id"] for d in self.get_deployments()]
            if len(deployments) == 1:
                self.set_default_deployment_id(deployments[0])
                return deployments[0]
            else:
                raise ValueError(
                    "User has access to multiple deployments, but no deployment has been specified. "
                    "Either pass a deployment as a parameter to methods supporting it, "
                    "or set it through the `set_default_deployment` method. "
                    "Available deployments can be retrieved with `get_deployments`."
                )
        return self._default_deployment_id

    @lru_cache()
    def get_reading_types(self, deployment_id: Union[str, None]=None):
        """Return reading types available to the user. If deployment_id not passed, a default deployment must be set
        with `set_default_deployment_id`. If the user only has access to one deployment, it is automatically set
        as a default deployment."""
        if deployment_id is None:
            deployment_id = self.get_default_deployment_id()
        return self._request_with_token(f"{self.host}/v0/deployments/{deployment_id}/devices/readings")

    @lru_cache()
    def get_deployments(self):
        """Return deployments accessible to the user."""
        return self._request_with_token(f"{self.host}/v0/deployments")

    @lru_cache()
    def get_positions(
            self,
            deployment_id: Union[None, str] = None,
            limit: Union[None, int] = None,
    ):
        """Return device positions.

        :param deployment_id: deployment for which to get readings. Defaults to None.
            If None:

                * If a user only has access to a single deployment, it will automatically be set as
                 the default deployment.

                * If a user has access to more than one deployment, and deployment is None,
                 a default deployment has to be set with `set_default_deployment_id`,
                 or an error will be raised.
        :param limit: limit the number of results per page. Only applied when smaller than API's own internal limit.
        :return: dictionary of results.
        """

        params = {}
        if limit is not None:
            params["limit"] = limit

        if deployment_id is None:
            deployment_id = self.get_default_deployment_id()

        return self._get_with_cursor(
            f"{self.host}/v0/deployments/{deployment_id}/devices/positions",
            params
        )



    def get_readings(
        self,
        reading_type: str,
        deployment_id: Union[None, str] = None,
        from_time: Union[None, datetime] = None,
        to_time: Union[None, datetime] = None,
        limit: Union[None, int] = None,
    ):
        """Get readings

        :param reading_type: a reading type as present in the list returned from get_available_reading_types
        :param deployment_id: deployment for which to get readings. Defaults to None.
            If None:

                * If a user only has access to a single deployment, it will automatically be set as
                 the default deployment.

                * If a user has access to more than one deployment, and deployment is None,
                 a default deployment has to be set with `set_default_deployment_id`,
                 or an error will be raised.

        :param from_time: inclusive earliest bound of time range for which to get data. When None, include available
          data from the last day.
        :param to_time: inclusive latest bound of time range for which to get data. When None, include available
          data until the current time.
        :param limit: limit the number of results per page. Only applied when smaller than API's own internal limit.
        :return: dictionary of results.
        """
        params = {}

        if deployment_id is None:
            deployment_id = self.get_default_deployment_id()

        if from_time is None:
            from_time = datetime.now(tz=timezone.utc) - timedelta(days=1)
        params["from"] = int(from_time.astimezone(tz=timezone.utc).timestamp())

        if to_time is not None:
            params["to"] = int(to_time.astimezone(tz=timezone.utc).timestamp())
        if limit is not None:
            params["limit"] = limit

        results = self._get_with_cursor(
            f"{self.host}/v0/deployments/{deployment_id}/devices/readings/{reading_type}",
            params
        )

        # date string to datetime object
        for datapoint in results:
            datapoint["time_created"] = datetime.fromisoformat(datapoint["time_created"])

        return results

