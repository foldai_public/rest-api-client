from setuptools import setup, find_packages

EXTRAS = {
    "tests": {
        "pytest",
        "black",
        "pytest-runner",
        "pytest-black",
    }
}


setup(
    name="foldAI_api",
    version="0.0.1",
    description="Client for foldAI REST API",
    packages=find_packages(),
    install_requires=[
        "requests >= 2.26.0",
    ],
    extras_require=EXTRAS,
)
