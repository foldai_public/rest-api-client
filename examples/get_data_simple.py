"""Sample code for simple log in and getting readings data in time window"""
import os
from datetime import datetime, timedelta, timezone
from pprint import pprint

from foldAI_api import Client

client = Client(
    user_email=os.environ["FOLDAI_API_EMAIL"],
    user_password=os.environ["FOLDAI_API_PASSWORD"],
)

available_reading_types = client.get_reading_types()
print("Available reading types for this user and deployment are:")
print("".join(f"- {d}\n" for d in available_reading_types))

positions = client.get_positions()
print("Positions:\n")
pprint(positions)

# Select e.g. first available reading type
readings = client.get_readings(
    reading_type=available_reading_types[0],
    from_time=datetime.now(tz=timezone.utc) - timedelta(days=3),
    to_time=datetime.now(tz=timezone.utc) - timedelta(days=1)
)
print("Readings data:\n")
pprint(readings)

