# REST API client

This repository contains a Python3 client for foldAI's REST API.

### Prerequisites

A system with a Python3 installation, and an up-to-date version of the Python3 `pip` package manager.
We also recommend setting up a virtual environment for the project.

### Installation

While inside the `src` folder of this project, install the client library by running:

```python
pip install .
```

### Examples

In the `examples` folder, you will find runnable code to create a Client and interact with the API.
